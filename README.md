#Research Proposal : Many-objective evolutionary diversity optimization#

The template of research proposals is restricted to edit or add figures. Therefore, this repository has been created to upload figures and the Gantt chart that are essential for my research proposal.


---

### Figure 1: Eligible solutions for RHA

https://bitbucket.org/nikfarjamadel/research-proposal/downloads/Figure%201.png


---

### Figure 2: Schema of the proposed methodology

https://bitbucket.org/nikfarjamadel/research-proposal/downloads/Figure%202.png


---

### Gantt chart

https://bitbucket.org/nikfarjamadel/research-proposal/downloads/gantt-chart-template-3y.xlsx


---

### The longer version of the research proposal

https://bitbucket.org/nikfarjamadel/research-proposal/downloads/research%20proposal-final.pdf

